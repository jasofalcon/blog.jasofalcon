import React, { Component } from 'react';
import Cover from './layout/Cover';
import CoverIcon from './layout/CoverIcon';
import CoverIconImage from './layout/CoverIconImage';
import ShortBio from './layout/ShortBio';
import AboutContent from './about/AboutContent';
import Helmet from 'react-helmet';

class About extends Component {
  render() {
    return (
      <div>
        <Helmet title="About &bull; JasoFalcon" />
        <Cover dimmedOpacity={0.2} imageUrl="about.jpg" />
        <CoverIcon>
          <CoverIconImage imageUrl="portrait.jpg" size="130" />
        </CoverIcon>
        <ShortBio />
        <AboutContent />
      </div>
    );
  }
}

export default About;
