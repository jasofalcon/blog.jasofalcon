import React, { Component } from "react";
import * as contentful from "contentful";
import Cover from "./layout/Cover";
import CoverIcon from "./layout/CoverIcon";
import CoverIconImage from "./layout/CoverIconImage";
import ShortBio from "./layout/ShortBio";
import PostContent from "./blog/PostContent";
import Helmet from "react-helmet";

export default class BlogPost extends Component {
  state = {
    post: {}
  };

  postId = this.props.match.params.postId;

  client = contentful.createClient({
    space: "tfnpiikh8oyq",
    accessToken:
      "afc7db602f028ce4de838326ea5e5d074a57da7b93fe0d7bde2939a8a44ea39c"
  });

  componentDidMount() {
    this.fetchPost().then(this.setPost);
  }

  fetchPost = () =>
    this.client.getEntries({
      "fields.path[match]": this.postId,
      content_type: "blogPost"
    });

  setPost = response => this.setState({ post: response.items[0] });

  render() {
    if (!this.state.post.fields) {
      return <div>loading...</div>;
    }

    const fields = this.state.post.fields;

    return (
      <div>
        <Helmet title={fields.title + " • JasoFalcon"} />
        <Cover imageUrl={"/posts-assets/" + fields.coverUrl} />
        <CoverIcon>
          <CoverIconImage imageUrl="falcon-logo-white.png" />
        </CoverIcon>
        <ShortBio />
        <PostContent {...fields} />
      </div>
    );
  }
}
