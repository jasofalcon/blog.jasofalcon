import React, { Component } from 'react';
import PageTitle from '../layout/PageTitle';
import Page from '../layout/Page';

class AboutContent extends Component {
  render() {
    return (
      <Page>
        <PageTitle>About</PageTitle>
        <br />
        <p>
          Hi there, stranger. My name's Janko and I come from Belgrade, Serbia.
          I've been in love with technology ever since I can remember. The lines
          I write here are mostly about various topics that caught my attention
          when I wasn't drinking beer.
          <br />
          <br />
          I've got 6 years experience, mainly in Java and JavaScript stack.
          However I must admit to have some experience and even a startup in PHP
          as well. Forgive me for my shady past. My mom won't.
          <br /> You can check it out but don't judge &nbsp;
          <a
            href="https://ispovesti.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            ispovesti.com
          </a>
          <br />
          <br />
          I'm currently employed at Zuhlke Engineering, solving various problems
          on different projects.
          <br />
          <br />
          On a personal side, I enjoy travelling and visiting places where food
          and culture would surprise me in same way that the rain won't. I
          rarely say `No` to any kind of ball sport, game night or a good movie.
          <br />
          <br />
          If you have any comments or inquiries, feel free to drop me a message
          on me@jasofalcon.com. :&#41;
        </p>
      </Page>
    );
  }
}

export default AboutContent;
