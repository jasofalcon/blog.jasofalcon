import React from "react";
import PropTypes from "prop-types";
import Helmet from "react-helmet";

import Site from "./layout/Site";
import Header from "./layout/Header";
import Content from "./layout/Content";
import Footer from "./layout/Footer";
import Router from "./layout/Router";

const Layout = () => (
  <Site>
    <Helmet
      title="Home &bull; JasoFalcon"
      meta={[
        {
          name: "description",
          content: "Just another software engineer's point of view"
        },
        {
          name: "keywords",
          content: "resume, blog, jasofalcon, software, it, engineering, tech"
        }
      ]}
    >
      <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
        integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"
        crossorigin="anonymous"
      />
    </Helmet>
    <Header />
    <Content>
      <Router />
    </Content>
    <Footer />
  </Site>
);

Layout.propTypes = {
  children: PropTypes.func
};

export default Layout;
