import React from "react";
import * as Markdown from "react-markdown";
import styled from "styled-components";

const BlogItemWrapper = styled.div`
  border-left: 2px solid lightgray;
  padding-left: 20px;
  color: #666;
  display: flex;
  flex-direction: column;
  border-bottom: 1px solid #eeeeee;
  padding-top: 20px;
`;

const BlogItemTitle = styled.a`
  font-size: 32px;
  font-weight: 600;
  color: #222;
  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
`;

const TimeStamp = styled.div`
  position: absolute;
  margin-top: 9px;
  margin-left: -134px;
  width: 100px;
  text-align: right;
  font-size: 16px;
  opacity: 0.9;
  @media only screen and (max-width: 600px) {
    & {
      position: relative;
      margin: 10px 0 0 0;
      text-align: left;
    }
  }
`;

const TimePoint = styled.div`
  width: 13px;
  height: 13px;
  border-radius: 50%;
  background-color: lightgray;
  margin-left: -36px;
  position: absolute;
  margin-left: -32px;
  margin-top: 8px;
  background: grey;
  border: 5px solid white;
  &.Green {
    background: green;
  }
`;

const formatDate = date =>
  new Date(date).toLocaleDateString("en-au", {
    year: "numeric",
    month: "short",
    day: "numeric"
  });

const BlogItem = props => (
  <BlogItemWrapper>
    <BlogItemTitle href={"#/post/" + props.path}>{props.title}</BlogItemTitle>
    <TimePoint className={props.id === 0 ? "Green" : ""} />
    <TimeStamp>{formatDate(props.timestamp)}</TimeStamp>
    <Markdown
      source={props.content
        .split(" ")
        .splice(0, 100)
        .join(" ")
        .concat("...")}
    />
  </BlogItemWrapper>
);

export default BlogItem;
