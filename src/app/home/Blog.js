import React, { Component } from "react";
import * as contentful from "contentful";

import BlogItem from "./BlogItem";
import Page from "../layout/Page";
class Blog extends Component {
  state = {
    posts: []
  };

  client = contentful.createClient({
    space: "tfnpiikh8oyq",
    accessToken:
      "afc7db602f028ce4de838326ea5e5d074a57da7b93fe0d7bde2939a8a44ea39c"
  });

  componentDidMount() {
    this.fetchPosts().then(this.setPosts);
  }

  fetchPosts = () => this.client.getEntries();

  setPosts = response => {
    this.setState({ posts: response.items });
  };

  generateBlogItems() {
    return this.state.posts.map(({ fields }, i) => (
      <BlogItem key={i} {...fields} id={i} />
    ));
  }

  render() {
    return (
      <Page>
        <br />
        {this.generateBlogItems()}
      </Page>
    );
  }
}

export default Blog;
