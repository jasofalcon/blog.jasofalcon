import React, { Component } from "react";

/**
 * Integration of widgetpack comments
 */
export default class PostComments extends Component {
  componentDidMount() {
    this.initializeCommentsWidget();
  }

  initializeCommentsWidget() {
    window.wpac_init = window.wpac_init || [];
    window.wpac_init.push({
      widget: "Comment",
      id: 12392
    });
    (function() {
      if ("WIDGETPACK_LOADED" in window) return;
      window.WIDGETPACK_LOADED = true;
      var mc = document.createElement("script");
      mc.type = "text/javascript";
      mc.async = true;
      mc.src = "https://embed.widgetpack.com/widget.js";
      var s = document.getElementsByTagName("script")[0];
      s.parentNode.insertBefore(mc, s.nextSibling);
    })();
  }

  render() {
    return <div id="wpac-comment" />;
  }
}
