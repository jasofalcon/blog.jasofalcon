import React, { Component } from 'react';
import styled from 'styled-components';
import * as Markdown from 'react-markdown';
import PostComments from './PostComments';
import Page from '../layout/Page';
import PageTitle from '../layout/PageTitle';

const BlogText = styled.div`
  font-size: 21px;
  color: #444;
  font-weight: 300;
  letter-spacing: 0.01em;
  line-height: 1.58;
  margin-bottom: 50px;
  & pre {
    background: #eff0f1;
    padding: 10px;
    border-radius: 5px;
    font-size: 16px;
    code {
      font-family: 'Source Code Pro', monospace !important;
      color: color: #444;
      white-space: pre-wrap;
      font-size: 16px;
      word-wrap: break-word;
    }
  }
`;

const BlogTimestamp = styled.div`
  margin: 5px 0 25px 0;
  color: grey;
`;

const formatDate = date =>
  new Date(date).toLocaleDateString('en-au', {
    year: 'numeric',
    month: 'short',
    day: 'numeric'
  });

export default class PostContent extends Component {
  render() {
    return (
      <Page>
        <PageTitle>{this.props.title}</PageTitle>
        <BlogTimestamp>{formatDate(this.props.timestamp)}</BlogTimestamp>
        <BlogText>
          <Markdown escapeHtml={false}>{this.props.content}</Markdown>
        </BlogText>
        <PostComments />
      </Page>
    );
  }
}
