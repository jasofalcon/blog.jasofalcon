import React from "react";
import styled from "styled-components";
import SocialLinks from "./SocialLinks";

const ShortBioWrapper = styled.div`
  text-align: center;
  color: #333;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const ShortBio = () => (
  <ShortBioWrapper>
    <br />
    <div>Janko Sokolović</div>
    <div>Software Engineer</div>
    <br />
    <SocialLinks />
  </ShortBioWrapper>
);

export default ShortBio;
