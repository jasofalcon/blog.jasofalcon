import styled from "styled-components";

const CoverIconImage = styled.div`
  background: url(/${props => props.imageUrl});
  width: 100px;
  height: 100px;
  background-size: ${props => (props.size ? props.size : "70")}px;
  background-color: dimgrey;
  background-position:center
  background-repeat: no-repeat;
  border-radius: 50%;
  position: absolute;
  left: 5px;
  top: 5px;
`;

export default CoverIconImage;
