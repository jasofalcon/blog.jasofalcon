import React from "react";
import styled from "styled-components";
import SocialLinks from "../layout/SocialLinks";

const FooterWrapper = styled.footer`
  padding: 20px;
  background: #fafafa;
  display: flex;
  justify-content: space-between;

  color: #aaaaaa;
  a {
    color: #aaaaaa;
    &:hover {
      color: #444;
    }
  }
`;

const SocialLinksWrapper = styled.div`
  text-align: left;
  flex: 1;
`;

const RepoLink = styled.a`
  flex: 1;
  text-align: center;
`;

const Copyright = styled.div`
  flex: 1;
  text-align: right;
`;

const HeartIcon = styled.span`
  width: 15px;
  height: 15px;
  display: inline-block;
  opacity: 0.5;
  margin: 0 5px;
  background: url("/icons/heart-solid.svg");
`;

const Footer = () => (
  <FooterWrapper className="footer">
    <SocialLinksWrapper>
      <SocialLinks />
    </SocialLinksWrapper>
    <RepoLink
      href="https://gitlab.com/jasofalcon/blog.jasofalcon"
      target="_blank"
    >
      built with <HeartIcon />
    </RepoLink>
    <Copyright>&copy; 2018</Copyright>
  </FooterWrapper>
);

export default Footer;
