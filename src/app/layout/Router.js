import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../Home';
import BlogPost from '../BlogPost';
import About from '../About';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-120838767-1');

const logPageView = () => {
  ReactGA.set({ page: window.location.pathname + window.location.hash });
  ReactGA.pageview(window.location.pathname + window.location.hash);
  return null;
};

const Router = () => (
  <div>
    <Route path="/" component={logPageView} />
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/post/:postId" component={BlogPost} />
      <Route path="/about/" component={About} />
    </Switch>
  </div>
);

export default Router;
