import styled from "styled-components";

const CoverIcon = styled.div`
  position: absolute;
  width: 110px;
  height: 110px;
  border-radius: 50%;
  margin: 0 auto;
  position: relative;
  margin-top: -50px;
  background: white;
`;

export default CoverIcon;
