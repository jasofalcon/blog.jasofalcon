import React from "react";
import styled from "styled-components";

const SocialWrapper = styled.div`
  display: flex;
  opacity: 0.5;
`;

const Social = styled.a`
  width: 20px;
  height: 20px;
  margin: 0 10px;
  background-repeat: no-repeat;
  &:hover {
    opacity: 0.75;
  }
  &.Github {
    background-image: url("/icons/github.svg");
  }
  &.Gitlab {
    background-image: url("/icons/gitlab.svg");
  }
  &.Twitter {
    background-image: url("/icons/twitter.svg");
  }
`;

const SocialLinks = () => (
  <SocialWrapper>
    <Social className="Github" href="https://github.com/jasofalcon" /> ·
    <Social className="Gitlab" href="https://gitlab.com/jasofalcon" /> ·
    <Social className="Twitter" href="https://twitter.com/jasofalcon" />
  </SocialWrapper>
);

export default SocialLinks;
