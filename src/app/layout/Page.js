import styled from "styled-components";

const Page = styled.div`
  max-width: 740px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  padding: 40px;
  font-size: 21px;
  font-weight: 100;
  letter-spacing: 0.01em;
  color: #353535;
`;

export default Page;
