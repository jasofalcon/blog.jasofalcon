import React, { Component } from "react";
import styled from "styled-components";

const CoverImage = styled.div`
  width: 100%;
  height: 600px;
  background-image: url("${props =>
    props.imageUrl ? props.imageUrl : "cover.jpg"}");
  background-size: cover;
  background-position: center;
  @media only screen and (max-width: 600px) {
    height: 400px;
  }
`;

const CoverOverlay = styled.div`
  background: black;
  opacity: ${props => (props.dimmedOpacity ? props.dimmedOpacity : "0")};
  height: 100%;
`;

class Cover extends Component {
  render() {
    return (
      <CoverImage imageUrl={this.props.imageUrl}>
        <CoverOverlay dimmedOpacity={this.props.dimmedOpacity} />
      </CoverImage>
    );
  }
}

export default Cover;
