import React, { Component } from "react";
import styled from "styled-components";

const HeaderWrapper = styled.div`
  padding: 0 20px;
  position: absolute;
  width: 100%;
  box-sizing: border-box;
  z-index: 1;
`;

const HeaderContent = styled.div`
  max-width: 1000px;
  height: 65px;
  display: flex;
  flex-direction: row;
  justify-content: left;
  margin: 0 auto;
  flex: 1 1 auto;
`;

const NavItem = styled.a`
  color: #ffffff;
  text-decoration: none;
  line-height: 65px;
  margin: 0 10px;
`;
class Header extends Component {
  render() {
    return (
      <HeaderWrapper>
        <HeaderContent>
          <NavItem href="/">Home</NavItem>
          <NavItem href="#/about">About me</NavItem>
        </HeaderContent>
      </HeaderWrapper>
    );
  }
}

export default Header;
