import styled from "styled-components";

const PageTitle = styled.div`
  font-size: 42px;
  font-weight: 900;
`;

export default PageTitle;
