import React, { Component } from "react";
import Blog from "./home/Blog";
import CoverIcon from "./layout/CoverIcon";
import CoverIconImage from "./layout/CoverIconImage";
import ShortBio from "./layout/ShortBio";
import Cover from "./layout/Cover";

class Home extends Component {
  render() {
    return (
      <div>
        <Cover dimmedOpacity={0.3} />
        <CoverIcon>
          <CoverIconImage imageUrl="falcon-logo-white.png" />
        </CoverIcon>
        <ShortBio />
        <Blog />
      </div>
    );
  }
}

export default Home;
